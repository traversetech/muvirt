# Guidelines for building ARM Virtual Machine images

Virtualization capabilities exist in ARMv8 (64-bit) processors as well as some versions of the ARMv7 (32-bit) ISA.

The advent of ARM processors targeted at the datacenter market (Qualcomm Centriq, Cavium ThunderX, Applied Micro XGene) means standards (such as the ARM Server Base Specification) were desired to ensure capability across the individual offerings.

Hence there are two specifications of interest: The ARM [Server Base Boot Requirements](https://developer.arm.com/docs/den0044/latest/server-base-boot-requirements-system-software-on-arm-platforms) and the [Linaro VM System Specification](https://www.linaro.org/blog/vm-system-specification-v2-arm-processors/).

SBBR mandates two important differences from traditional ARM Embedded:

* UEFI for boot and certain runtime services (such as real time clock, reboot etc.)
* ACPI for hardware descriptions (as opposed to device tree).

A further development is the [Embedded Base Boot Requirements](https://developer.arm.com/products/architecture/system-architecture/embedded-system-architecture)
which define a subset of SBAS for embedded systems (for example, using device tree in place of ACPI is supported).

Given the use of UEFI and ACPI, a virtual machine can be constructed for ARM in much the same way as on x86, noting:
* The VM disk must be formatted as GPT, no legacy MBR is required
* The VM disk must have a boot/EFI system partition, formatted as vfat/FAT32, and at least 128MB in size.
* The EFI binary must be place in /EFI/boot/bootaa64.efi on the EFI system partition, this is the standard location for “removable media”.
GRUB2 can be used as the boot loader, in much the same way as x86, or a Linux kernel can be booted as an EFI binary directly, with EFI boot stub support.

GRUB2 can be installed (config and module files) on the EFI system partition, but most mainstream distributions follow the x86 practice of placing GRUB2 files on an ext4 /boot partition.

Note that CONFIG_EFI_STUB must be enabled in the kernel, even if GRUB2 is used.

Also be aware:

* ARM server machines and VM’s use the ARM PrimeCell serial UART, /dev/ttyAMA0. 
    Linux 4.14 onwards will automatically set ttyAMA0 as the early console (as directed by ACPI), but you will still need to set userspace (such as /etc/inittab for non-systemd) do use it. When cross compatibility with non-SBAS systems (NXP LS processors) is required, it is ok to specify multiple console= targets on the kernel command line (for example, console=ttyAMA0,115200 console=ttyS0,115200)
* To ensure portability between VM’s, physical servers and/or different storage systems (NVMe, MMC, USB, SATA), use partition UUIDs in place of “real” device names where possible, for example:

    In GRUB:
```linux /vmlinuz-4.14.0-00025-g0332baf2c73c root=PARTUUID=ac5563ea-4ec3-4fb8-b018-3e1376b13445 ro  console=ttyS0,115200 rootwait net.ifnames=0```

    In fstab:
    ```
PARTUUID=ac5563ea-4ec3-4fb8-b018-3e1376b13445           /               ext4    defaults        1       1
PARTUUID=3f8c3ebd-f3da-4bd4-856a-dac6d55d94ad           /boot   ext4    defaults        1       1
PARTUUID=2cc805c0-4199-428b-8706-d701d3afcd4f           /boot/efi vfat defaults 1       1
```

    PARTUUID’s for existing partitions can be identified with blkid:
    ```
[root@fedora-arm64 ~]# blkid /dev/vda2
/dev/vda2: UUID="9a07eea9-37ea-4f63-9cbf-1701610a0d87" TYPE="ext4" PARTLABEL="boot" PARTUUID="3f8c3ebd-f3da-4bd4-856a-dac6d55d94ad"
```

    If the partition is changed, i.e resized, PARTUUID will change and this can break the system configuration. PARTUUIDs can be changed back with sgdisk:
    ```
sgdisk –partition-guid=1:NEW-PARTUUID /dev/vda
``` 
   will change the PARTUUID for /dev/vda1
    Another possibility is to use boot by partition label (root=LABEL=…), however, this requires a initramfs to do the actual device search. 

## Starting and testing ARM VMs

QEMU can be used both for native virtualization and to emulate a ARM system on a non-ARM host (x86). Use the following command to launch a virtual machine on x86:
``` 
sudo qemu-system-aarch64 -m 1024 -cpu cortex-a57 -M virt -nographic -bios "QEMU_EFI.fd" \
	-drive if=none,file=armvm.qcow2,id=hd0  -serial tcp::4446,server,telnet -monitor stdio \
	-device virtio-blk-device,drive=hd0
``` 
On a native ARMv8 system, use -cpu host:
```
sudo qemu-system-aarch64 -m 1024 -cpu host -M virt -nographic -bios "QEMU_EFI.fd" \
	-drive if=none,file=armvm.qcow2,id=hd0  -serial tcp::4446,server,telnet -monitor stdio \
	-device virtio-blk-device,drive=hd0
```

In some situations, the interrupt controller (GIC) version may need to be specified as v2 or v3 (ThunderX host):

```
qemu-system-aarch64 -M virt,gic_version=2
```

At the time of writing, GIC version 2 is the default when -M virt is specified.

A EFI/TianoCore binary is required (QEMU_EFI.fd), you can download one from here:
http://snapshots.linaro.org/components/kernel/leg-virt-tianocore-edk2-upstream/latest/QEMU-AARCH64/RELEASE_GCC5/

## Using u-boot to boot EFI targets
u-boot, since mid-2017 has EFI capabilities, via the bootefi command. A u-boot target with distroboot support will search for /efi/boot/bootaa64.efi as part of its search process.

For more information, see the paper [UEFI on Top of U-Boot](https://www.suse.com/docrep/documents/a1f0ledpbe/UEFI%20on%20Top%20of%20U-Boot.pdf).
* Note that a device tree file needs to be loaded/selected (fdt addr $fdt_addr_r) first.
* When GRUB is used as the EFI binary, the u-boot bootargs= are overwritten by GRUB
* U-boot bootscripts take precedence over EFI, to allow for fast boot times 
on embedded targets you could construct a ‘hybrid’ image with a u-boot bootscript for your supported targets, as well as GRUB2/EFI for VM and server use. The same kernel can be used for both.
