#!/bin/sh
set -e

# For CI permissions/umask troubleshooting
id
ls -la . | head -n 5

MUVIRT_BUILD_TARGET=$1
if [ ! -f "configs/${MUVIRT_BUILD_TARGET}" ]; then
	echo "Target must be supplied"
	echo "Currently defined targets are: "
	ls configs/
	exit 1
fi
set -e

if [ ! -f muvirt-feed/muvirt/Makefile ] || [ ! -f muvirt-lede/Makefile ]; then
	echo "ERROR: submodules muvirt-feed and/or muvirt-lede are not present"
	echo "Please run \"git submodule update --init\" or re-clone this repository with the --recursive flag"
	exit 1
fi

export TEN64_FEED_URL=${TEN64_FEED_URL:-https://gitlab.com/traversetech/ls1088firmware/ten64-openwrt-feed.git}

cat > muvirt-lede/feeds.conf <<EOF
src-link muvirt $(readlink -f muvirt-feed)
src-git packages https://github.com/openwrt/packages.git;openwrt-24.10
src-git luci https://github.com/openwrt/luci.git;openwrt-24.10
src-git openwisp https://github.com/openwisp/openwisp-config.git
src-git ten64 ${TEN64_FEED_URL}
EOF

echo "Building with this feed configuration: "
cat muvirt-lede/feeds.conf

cp "configs/${MUVIRT_BUILD_TARGET}" muvirt-lede/wrt-config
DOWNLOAD_DIR=$(readlink -f downloads)
echo "CONFIG_DOWNLOAD_FOLDER=\"$DOWNLOAD_DIR\"" >> muvirt-lede/wrt-config

if [ ! -z "${BUILD_PACKAGE_URL}" ]; then
	VERSION_REPO_URL=$(echo "${BUILD_PACKAGE_URL}" | sed "s/_BRANCH_/${CI_COMMIT_REF_NAME}/g" | sed "s/_PIPELINE_/${CI_PIPELINE_ID}/g")
	echo "CONFIG_VERSION_REPO=\"${VERSION_REPO_URL}\"" >> muvirt-lede/wrt-config
	echo "${VERSION_REPO_URL} will be set as the opkg url in the generated images"
fi

cd muvirt-lede
./scripts/feeds clean
./scripts/feeds update -a
./scripts/feeds install -p muvirt -a
./scripts/feeds install -p packages -a
./scripts/feeds install -p luci -a
./scripts/feeds install tpm-tools
./scripts/feeds install trousers
./scripts/feeds install kmod-hwmon-traverse-sensors
./scripts/feeds install ten64-controller-util
./scripts/feeds install -p openwisp openwisp-config


# When the build is done inside gitlab-ci, the remote URLs for the git repos include
# the (temporary) gitlab CI token, which eventually leaks into the SDK.
# Strip the tokens so the public URLs appear.

if [ -n "${CI}" ]; then
	MUVIRT_LEDE_REMOTE=$(git remote get-url origin)
	MUVIRT_LEDE_PUBLIC=$(echo "${MUVIRT_LEDE_REMOTE}" | sed 's/https:\/\/.\+:.\+@/https:\/\//g')
	git remote set-url origin "${MUVIRT_LEDE_PUBLIC}"

	MUVIRT_FEED_REMOTE=$(git -C ../muvirt-feed remote get-url origin)
	MUVIRT_FEED_PUBLIC=$(echo "${MUVIRT_FEED_REMOTE}" | sed 's/https:\/\/.\+:.\+@/https:\/\//g')
	git -C ../muvirt-feed remote set-url origin "${MUVIRT_FEED_PUBLIC}"

	# Now that feeds have been pulled, change the muvirt-feed url in feeds.conf to public version
	# otherwise, the generated SDK still has the "src-link muvirt ..." in feeds.conf
	grep -v "muvirt" feeds.conf > _feeds.conf
	echo "src-git muvirt ${MUVIRT_FEED_PUBLIC}" >> _feeds.conf
	mv _feeds.conf feeds.conf

	git config --global user.email "buildbot@example.com"
	git config --global user.name "CI Build please ignore"

	echo "CONFIG_VERSION_CODE=${CI_PIPELINE_ID}" >> wrt-config
fi

# Do a defconfig on a fresh tree right away, otherwise
# packages requiring deps from feeds will drop out in our
# config
make defconfig
cp wrt-config .config
make defconfig

grep "CONFIG_PACKAGE_muvirt=y" .config || (echo "muvirt not selected" && exit 1)
grep "CONFIG_PACKAGE_luci=y" .config || (echo "LuCI not selected" && exit 1)
grep "CONFIG_PACKAGE_lvm2=y" .config || (echo "LVM not selected" && exit 1)
grep "CONFIG_PACKAGE_tmux=y" .config || (echo "tmux not selected" && exit 1)
grep "CONFIG_PACKAGE_qemu-img" .config || (echo "qemu-img not selected" && exit 1)
grep "CONFIG_PACKAGE_qemu-nbd" .config || (echo "qemu-nbd not selected" && exit 1)
grep "CONFIG_PACKAGE_kmod-hwmon-traverse-sensors" .config || (echo "kmod-hwmon-traverse-sensors not selected" && exit 1)
grep "CONFIG_PACKAGE_kmod-sfp=y" .config || (echo "kmod-sfp not selected" && exit 1)
grep "CONFIG_PACKAGE_kmod-gpio-pca953x=y" .config || (echo "kmod-gpio-pca953x not selected" && exit 1)
grep "CONFIG_PACKAGE_kmod-i2c-mux-pca954x=y" .config || (echo "kmod-i2c-mux-pca954x not selected" && exit 1)
grep "CONFIG_PACKAGE_ethtool-full=y" .config || (echo "ethtool-full not selected" && exit 1)
grep "CONFIG_PACKAGE_kmod-leds-gpio=y" .config || (echo "kmod-leds-gpio not selected" && exit 1)
grep "CONFIG_PACKAGE_kmod-rtc-rx8025=y" .config || (echo "kmod-rtc-rx8025 not selected" && exit 1)
grep "CONFIG_PACKAGE_openvpn-openssl=y" .config || (echo "openvpn-openssl not selected" && exit 1)
grep "CONFIG_PACKAGE_luci-app-openvpn=y" .config || (echo "luci-app-openvpn not selected" && exit 1)
grep "CONFIG_PACKAGE_luci-proto-wireguard=y" .config || (echo "luci-proto-wireguard not selected" && exit 1)
grep "CONFIG_PACKAGE_qrencode=y" .config || (echo "qrencode not selected" && exit 1)

echo "Building with `nproc` cores"
make -j$(nproc)

# Avoid delivering files with "+" (<space> in URL encoding)
for i in $(find bin/targets -maxdepth 3 -type f -name '*+*'); do
	newname=$(echo "${i}" | sed 's/\+/-/g')
	mv "${i}" "${newname}"
done

# Fix names in SHA256SUMs as well
SHA256SUMS_FILE=$(find bin -name sha256sums)
sed -i 's/\+/-/g' "${SHA256SUMS_FILE}"
echo "Build done"
echo "-------------------------------------------------------------------------"
echo "SHA256SUMs:"
cat "${SHA256SUMS_FILE}"
