#!/usr/bin/env python3

import os
import sys
import json
from json import JSONEncoder

import argparse


class ImageRegistryEntry:
    def __init__(self, newid):
        self.idstr = newid
        self.name = None
        self.description = None
        self.maintainer = None
        self.download = None
        self.link = None
        self.checksum = None
        self.format = None
        self.version = None
        self.mindisk = None
        self.supported_hardware = []
        self.incompat_hardware = []
        self.cloudinit = False

    def add_supported_hardware(self, hardwareid):
        self.supported_hardware.append(hardwareid)

    def add_incompat_hardware(self, hardwareid):
        self.incompat_hardware.append(hardwareid)

class ImageRegistryEntryEncoder(JSONEncoder):
    def default(self, obj):
        if(isinstance(obj, ImageRegistryEntry)):
            selfdict = obj.__dict__
            selfdict["id"] = obj.idstr
            del selfdict["idstr"]
            return selfdict
        return JSONEncoder.default(self, obj)

def read_image_sha256(checksumfile):
    computed_sha256file = open(checksumfile, 'r')
    computed_sha256data = computed_sha256file.read()
    sha256sum = computed_sha256data.split(" ")[0].strip()
    computed_sha256file.close()
    return sha256sum

def output_dictionary(outputfilename, entry):
    outputfile = open(outputfilename, 'w')
    json.dump(entry, outputfile, cls=ImageRegistryEntryEncoder)
    outputfile.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate a metadata file for the appliance store")
    parser.add_argument('id', help='The id for the appliance')
    parser.add_argument('imagefile', help='The image filename')
    parser.add_argument('name', help='The name of the appliance')
    parser.add_argument('description', help='The description of the appliance')
    parser.add_argument('version', help='Appliance version')
    parser.add_argument('downloadfile', help='')
    parser.add_argument('checksumfile', help='')
    parser.add_argument('outputfile', help='File to output metadata to')
    parser.add_argument('--hardware', help='Mark this appliance as for specific hardware')
    parser.add_argument('--incompat-hardware', help='Mark this appliance as *incompatible* with specific hardware')
    parser.add_argument('--maintainer', help="Maintainer")
    parser.add_argument('--format', default='qcow2', help='Format (default qcow2)')

    args = vars(parser.parse_args())

    idstr = args["id"]
    imagefile = args["imagefile"]
    name = args["name"]
    description = args["description"]
    version = args["version"]
    downloadfile = args["downloadfile"]
    checksumfile = args["checksumfile"]
    outputfile = args["outputfile"]

    entry = ImageRegistryEntry(idstr)
    entry.name = name
    entry.description = description
    entry.version = version
    entry.format = args["format"]
    entry.download = downloadfile

    if ("CI_PROJECT_URL" in os.environ):
        entry.link = os.environ["CI_PROJECT_URL"]

    if ("hardware" in args and args["hardware"] != None):
        entry.add_supported_hardware(args["hardware"])

    if ("incompat-hardware" in args and args["incompat-hardware"] != None):
        entry.add_incompat_hardware(args["incompat-hardware"])

    if ("maintainer" in args):
        entry.maintainer = args["maintainer"]

    sha256sum = read_image_sha256(checksumfile)
    entry.checksum = "sha256:{}".format(sha256sum)

    output_dictionary(outputfile, entry)
