#!/bin/sh
# Create a qcow2 version for appliance store / recovery write-image-disk
IMAGE_REALPATH=$(find ../muvirt-lede/bin/targets/armsr/armv8/ -name 'muvirt-*ext4-combined.img.gz')
IMAGES_DIR=$(dirname "${IMAGE_REALPATH}")
IMAGE_BASE_NAME=$(basename "${IMAGE_REALPATH}")

cp "${IMAGE_REALPATH}" .
IMAGE_BASE_NAME_NOGZ=$(echo "${IMAGE_BASE_NAME}" | rev | cut -c 4- | rev)
IMAGE_BASE_NAME_QCOW2="${IMAGE_BASE_NAME_NOGZ}.qcow2"
gunzip "${IMAGE_BASE_NAME}" || :
qemu-img convert -c -O qcow2 "${IMAGE_BASE_NAME_NOGZ}" "${IMAGE_BASE_NAME_QCOW2}"
sha256sum "${IMAGE_BASE_NAME_QCOW2}" > "${IMAGE_BASE_NAME_QCOW2}.sha256sum"

cp "${IMAGE_BASE_NAME_QCOW2}" "${IMAGES_DIR}"
cp "${IMAGE_BASE_NAME_QCOW2}.sha256sum" "${IMAGES_DIR}"
