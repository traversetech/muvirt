#!/bin/sh

# With ImageBuilder and SDK, the full bin/ dir exceeds the 1GiB upload limit
# on GitLab.com, so we upload it elsewhere for our archive server to grab
if [ -n "${CI}" ] && [ -n "${B2_UPLOAD_KEY_ID}" ] && [ -n "${B2_UPLOAD_KEY_VAL}" ]; then
	CONTINUE_UPLOAD=1
	(b2 account authorize "${B2_UPLOAD_KEY_ID}" "${B2_UPLOAD_KEY_VAL}" 1>/tmp/b2_auth_log.txt 2>&1) || {
	echo "B2 key id: ${B2_UPLOAD_KEY_ID}" >> /tmp/b2_auth_log.txt
	echo "B2 key cred: ${B2_UPLOAD_KEY_VAL}" >> /tmp/b2_auth_log.txt
	echo "ERROR: Failed to authorize b2 access for artefacts upload"
	echo "DEBUG: sleeping for 500 seconds for debug"
	sleep 500
	CONTINUE_UPLOAD=0
	}
	if [ "${CONTINUE_UPLOAD}" = "1" ]; then
		tar -C ../muvirt-lede/ -cf bindir.tar bin
		du -h bindir.tar
		xz -T0 bindir.tar
		du -h bindir.tar.xz
		JOB_FILE_NAME="project-${CI_PROJECT_ID}-${CI_JOB_ID}.tar.xz"
		mv bindir.tar.xz "${JOB_FILE_NAME}"
		sha256sum "${JOB_FILE_NAME}"
		b2 file upload "${B2_UPLOAD_BUCKET}" "${JOB_FILE_NAME}" "project-${CI_PROJECT_ID}/${JOB_FILE_NAME}"
	fi
fi