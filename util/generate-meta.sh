#!/bin/sh
set -e

# Generate appliance store metadata
export CI_PIPELINE_IID=${CI_PIPELINE_IID:-localbuild}
export CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME:-$(git rev-parse --abbrev-ref HEAD)}
BUILD_DATE=$(date +%Y%m%d)
BUILD_REV="${BUILD_DATE}-${CI_COMMIT_REF_NAME}_${CI_PIPELINE_IID}"

IMAGE_REALPATH=$(find ../muvirt-lede/bin/targets/armsr/armv8/ -name 'muvirt-*ext4-combined.img.qcow2')
[ ! -f "${IMAGE_REALPATH}" ] && \
  echo "ERROR: Could not find qcow2 image" && exit 1

IMAGES_DIR=$(dirname "${IMAGE_REALPATH}")
IMAGE_BASE_NAME=$(basename "${IMAGE_REALPATH}")

export APPSTORE_ID=${APPSTORE_ID:-muvirt}
export APPSTORE_NAME=${APPSTORE_NAME:-"muvirt (disk install)"}
export APPSTORE_DESCRIPTION=${APPSTORE_DESCRIPTION:-"Current muvirt build"}

./generate-metadata.py --maintainer="Traverse" --format="qcow2" --incompat-hardware="linux,virt" "${APPSTORE_ID}" "${IMAGE_REALPATH}" "${APPSTORE_NAME}" \
  "${APPSTORE_DESCRIPTION}" \
  "${BUILD_REV}" "${IMAGE_BASE_NAME}" "${IMAGE_REALPATH}.sha256sum" "${IMAGES_DIR}/appliance-metadata.json"

