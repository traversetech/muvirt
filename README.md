# μVirt - really small virtualization

Looking for builds for Traverse hardware? See [our build server](https://archive.traverse.com.au/pub/traverse/software/muvirt/).

μVirt (aka "micro"Virt or muVirt) is a small virtualization host built on top of OpenWrt, primarily
designed to host simple virtual network functions (VNF) on [universal CPE (uCPE)](https://www.sdxcentral.com/articles/contributed/understanding-use-universal-cpe/2017/07/)
boxes.

The current iteration works on 64-bit ARM hardware and virtual machines compliant
with the [Linaro VM System Specification](https://www.linaro.org/blog/vm-system-specification-v2-arm-processors/) (i.e VM images that use UEFI to boot)

## Major Features
* OpenWrt based, the "host" system can provide networking services to the guest VMs and local network.

    This is ideal for when you want a single appliance that does your local network routing as well as serving complex applications such as NAS, container stacks and private cloud.

    Being able to leverage OpenWrt's network stack makes μVirt ideal for customer-edge applications with cellular/4G/5G and WiFi.

* Small footprint - the system image (excluding VMs) fit in the onboard NAND. μVirt was originally developed for a board with only 2GB RAM!

* LuCI module for VM provisioning, management.

* Supports basic cloud-init configuration, such as setting the user password and SSH keys for VMs.

* Passthrough of USB devices, PCIe devices and DPAA2 containers.

* (Preview!) support for hardware acceleration and offload features, e.g NXP DPAA2, see [DPDMUX](https://gitlab.com/traversetech/muvirt/-/wikis/DPDMUX) for details.

    We are keen to research methods of running VMs that avoid the resource outlay of the OVS-DPDK traditionally used.

* Appliance Store interface for selecting and deploying VMs from major distributions.

## Comparisons with other VM hosts
- μVirt is not a replacement for OpenStack, oVirt, Proxmox or vSphere

    Think ESXi vs vSphere, HyperV vs SCCM. For example, μVirt hosts have local storage only.

- μVirt is not sold as "production ready" - but we do run "production" workloads with it ourselves. Use at own risk.

- Only serial consoles to the VM are supported, no VNC/SPICE support. Graphical console support is not planned.

    VMs compliant to the Linaro VM specification should have working consoles on ttyAMA0 (as do 'real' ARM servers/those complying with SBAS).

## Current supported hardware:
- Traverse LS1043 Family
- Traverse LS1088 Family (Ten64 and derivatives)

In theory, any virtualisation capable Aarch64 OpenWRT host should work, this means any CPU with GICv2 or later interrupt controller.
Use on other KVM-enabled platforms (x86, POWER) is possible but not planned at this stage.

At this time, only 64-bit hosts and VMs are supported (no Aarch32 support).

## Getting Started
Binary builds for Traverse hardware can be found at https://archive.traverse.com.au/pub/traverse/software/muvirt/.

μVirt is also available on the Ten64 [bare metal appliance store](https://ten64doc.traverse.com.au/software/appliancestore/).

The best way to get started is with the LuCI interface and the [Appliance Store](https://gitlab.com/traversetech/muvirt/-/wikis/ApplianceStoreQuickStart).

## Build notes:
This repository has two submodules (muvirt-lede and muvirt-feed), to obtain the μVirt sources do a
recursive clone:
```
git clone --recursive https://gitlab.com/traversetech/muvirt/
```

To build for the traverse-ls1043 target:
```
./build.sh traverse-ls1043
```

## Configuration example:
It is a good idea to set 'force_link' and 'empty_bridge' on all networks you intend to use with VM's, otherwise the bridge network might only come up when there is a physical connection, causing the VM to have an unconnected network interface.
```
cat /etc/config/network
	config network 'lan'
		option force_link '1'
		option bridge_empty '1'
		option type 'bridige'
		...
```
If you are using the 'lan' interface, and muvirt is not intended as the router for the local LAN, be sure to
disable dnsmasq:
```
/etc/init.d/dnsmasq disable
```

Here, LVM is used as the storage backend, and a LEDE VM is bridged to two outside interfaces.
```
config vm 'lede'
        option type 'aarch64'
        option cpu 'host'
        option memory 128
        option numprocs 1
        list mac '52:54:00:F4:A2:BD'
        list disks '/dev/virtlv/lededisk'
        list network 'privatelan privatewan'
        option enable 1

# cat /etc/config/network
config interface 'privatelan'
        option type 'bridge'
        option ifname 'eth2'
        option proto 'none'

config interface 'privatewan'
        option type 'bridge'
        option ifname 'eth3'
        option proto 'none'
```

### RNG/Random host configuration
A virtio-rng device is supplied to the VM, on some machines this may require the host RNG to be configured properly. As of 2020-01, rngd (from rng-tools) is enabled by default - an external RNG such as a TPM is used to kick the kernel RNG.

(Inbuilt mechanisms such as OpenWrt's `urngd` have been found to be insufficient for virtio-rng customers)

If your VM boots stall early in the boot process (just after GRUB, for example), this is most likely due to lack of entrophy in the host's RNG. (Check ```/proc/sys/kernel/random/entropy_avail```)

## Adding a VM
You can use ``muvirt-createvm`` to set up a VM quickly:
```
# Syntax
muvirt-createvm <vmname> <memory> <procs> <network> <disk>"

# For example
muvirt-createvm debian 512 1 lan /mnt/vm/debian.qcow2

# Start the VM
/etc/init.d/muvirt start debian
```

## Accessing VM consoles
Use `muvirt-console <vmname>` to spawn a tmux session to the child VM
Use the tmux disconnect sequence ( Ctrl-B ) to leave the session

_NOTE_: Do not attempt to use muvirt-console over the host's serial terminal, ```muvirt-console```
uses tmux which doesn't like /dev/console.

## Using USB Devices
Passthrough of USB devices is supported, this is useful for applications that require external hardware (e.g radios).

There are several ways to specify a USB device to passthrough:
* USB Serial Number

```
list usbdevice 'serial=ABCDEF'
```

* USB Vendor and Device ID
```
list usbdevice 'vendor=1027,product=24577'
```
Note the vendor IDs need to be converted from hex to decimal.

* USB Bus position (from `/sys/bus/usb/drivers/usb`)

```
list usbdevice 'device=4-1.2'
```

The USB device selector in the LuCI interface will attempt to select the most specific match for a device (USB serial if present, then vendor/device ID).

μVirt installs a [hotplug](https://openwrt.org/docs/guide-user/base-system/hotplug) handler (/etc/hotplug.d/usb/10-muvirt) so devices will be added/removed from the VM based on their physical status - if the device is missing at VM startup this will not prevent the VM from starting.

By default, a USB 3 (XHCI) controller is specified. If your board only has USB2 support, you will need to set ```virt.system.usbcontroller``` to 'usb-ehci'

### A note on passing through host UARTs (for IoT, uC/Arduino's etc.)
Unfortunately the ```virt``` machine model for ARM64 in QEMU only supports one serial port (used for the ttyAMA0) console so the passthrough of host
TTYs is not [currently possible](https://unix.stackexchange.com/questions/479085/can-qemu-m-virt-on-arm-aarch64-have-multiple-serial-ttys-like-such-as-pl011-t).
We have tried to use emulated 8250 devices under QEMU but these do not pass flow control signals, so may not be useful.
If your application depends on controlling outside things via UART, I2C or SPI, consider having a small host side proxy to do the hardware interfacing.
USB-serial controllers (e.g FTDI) will work with the USB passthrough functionality described above.

## Where to get virtual machine images
Most major distributions now publish "cloud images" for ARM64, which μVirt is designed to run.

The appliance store feature sources it's list of images from [arm-image-registry](https://gitlab.com/traversetech/arm-image-registry).

There is an list of "official" images from the major distributions, plus community built ones.

## Acknowledgements
Many thanks to the following people for their contributions:

* Thomas Niederprüm contributed support for USB passthrough, memory ballooning, virtio-rng and initial PCIe VFIO and DPAA2 passthrough.
